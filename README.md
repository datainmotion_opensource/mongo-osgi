## Mongo OSGi

This project provides OSGi components to use the MongoClientusing the currently OSGi-fied mongo driver.
There are several components to enable configuring the mongo client during runtime using the configuration admin service.

The project was inspired by Bryan Hunts eMongo:
https://github.com/BryanHunt/eMongo

Currently only Mongo driver version > 3.2 is supported in the code 