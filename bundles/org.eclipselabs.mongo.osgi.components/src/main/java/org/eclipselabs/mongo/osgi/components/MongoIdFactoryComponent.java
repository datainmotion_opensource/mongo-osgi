/**
 * Copyright (c) 2012 - 2016 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */
package org.eclipselabs.mongo.osgi.components;

import org.bson.types.ObjectId;
import org.eclipselabs.mongo.osgi.api.MongoIdFactory;
import org.eclipselabs.mongo.osgi.api.exceptions.MongoOSGiException;

/**
 * Returns the {@link ObjectId} as id. This is the default implementation of mongo
 * @author Mark Hoffmann
 * @since 06.05.2016
 */
public class MongoIdFactoryComponent extends AbstractComponent implements MongoIdFactory {

	/* 
	 * (non-Javadoc)
	 * @see org.eclipselabs.mongo.osgi.api.MongoIdFactory#getCollectionURI()
	 */
	@Override
	public String getCollectionURI() {
		return "*";
	}

	/* 
	 * (non-Javadoc)
	 * @see org.eclipselabs.mongo.osgi.api.MongoIdFactory#getNextId()
	 */
	@Override
	public Object getNextId() throws MongoOSGiException {
		return new ObjectId();
	}
	
}
