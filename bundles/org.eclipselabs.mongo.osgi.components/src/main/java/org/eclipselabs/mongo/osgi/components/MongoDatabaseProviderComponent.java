/**
 * 
 */

package org.eclipselabs.mongo.osgi.components;

import java.util.Map;

import org.eclipselabs.mongo.osgi.api.MongoClientProvider;
import org.eclipselabs.mongo.osgi.api.MongoDatabaseProvider;
import org.eclipselabs.mongo.osgi.components.helper.MongoComponentHelper;

import com.mongodb.client.MongoDatabase;

/**
 * Component for a mongo database provider.
 * @author bhunt
 * @author Mark Hoffmann
 */
public class MongoDatabaseProviderComponent extends AbstractComponent implements MongoDatabaseProvider
{
	private volatile String alias;
	private volatile String databaseName;
	private String uri;
	private MongoClientProvider mongoClientProvider;

	/**
	 * The OSGi DS activate callback
	 * @param properties the service properties
	 */
	public void activate(Map<String, Object> properties) {
		alias = (String) properties.get(PROP_ALIAS);
		handleIllegalConfiguration(MongoComponentHelper.validateProperty(alias, "database alias"));

		databaseName = (String) properties.get(PROP_DATABASE);
		handleIllegalConfiguration(MongoComponentHelper.validateProperty(databaseName, "database name"));

		uri = mongoClientProvider.getURIs()[0] + "/" + databaseName;
	}
	
	/**
	 * The OSGi DS de-activate callback
	 */
	public void deactivate() {
		
	}

	/* 
	 * (non-Javadoc)
	 * @see org.eclipselabs.mongo.osgi.api.MongoDatabaseProvider#getURI()
	 */
	@Override
	public String getURI() {
		return uri;
	}

	/* 
	 * (non-Javadoc)
	 * @see org.eclipselabs.mongo.osgi.api.MongoDatabaseProvider#getDatabase()
	 */
	@Override
	public MongoDatabase getDatabase() {
		MongoDatabase db = mongoClientProvider.getMongoClient().getDatabase(databaseName);
		return db;
	}

	/**
	 * @param mongoClientProvider
	 */
	public void bindMongoClientProvider(MongoClientProvider mongoClientProvider) {
		this.mongoClientProvider = mongoClientProvider;
	}
	
}
