package org.eclipselabs.mongo.osgi.integration.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.Dictionary;
import java.util.Hashtable;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import org.eclipselabs.mongo.osgi.api.MongoClientProvider;
import org.eclipselabs.mongo.osgi.api.MongoDatabaseProvider;
import org.eclipselabs.mongo.osgi.configuration.ConfigurationProperties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.BundleException;
import org.osgi.framework.FrameworkUtil;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.framework.ServiceReference;
import org.osgi.service.cm.Configuration;
import org.osgi.service.cm.ConfigurationAdmin;
import org.osgi.util.tracker.ServiceTracker;

public class MongoClientProviderOSGiTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testCreateMongoClientProvider() throws InvalidSyntaxException, BundleException, IOException, InterruptedException {
		// get some of the bundles to test, to get the bundle context
		Bundle bundle = FrameworkUtil.getBundle(MongoDatabaseProvider.class);
		assertEquals("org.eclipselabs.mongo.osgi.api", bundle.getSymbolicName());
		bundle.start();
		assertEquals(Bundle.ACTIVE, bundle.getState());
		
		// get bundle context
		BundleContext context = bundle.getBundleContext();
		assertNotNull(context);
		
		// service lookup for configuration admin service
		ServiceReference<?>[] allServiceReferences = context.getAllServiceReferences(ConfigurationAdmin.class.getName(), null);
		assertNotNull(allServiceReferences);
		assertEquals(1, allServiceReferences.length);
		ServiceReference<?> cmRef = allServiceReferences[0];
		Object service = context.getService(cmRef);
		assertNotNull(service);
		assertTrue(service instanceof ConfigurationAdmin);
		
		// create mongo client configuration
		ConfigurationAdmin cm = (ConfigurationAdmin) service;
		Configuration clientConfig = cm.getConfiguration(ConfigurationProperties.CLIENT_PID);
		assertNotNull(clientConfig);
		
		// has to be a new configuration
		Dictionary<String, Object> p = clientConfig.getProperties();
		assertNull(p);
		
		// add service properties
		String clientId = "testClient";
		String clientUri = "mongodb://localhost:27017";
		p = new Hashtable<String, Object>();
		p.put(MongoClientProvider.PROP_CLIENT_ID, clientId);
		p.put(MongoClientProvider.PROP_URI, clientUri);
		clientConfig.update(p);
		
		// re-check configuration
		clientConfig = cm.getConfiguration(ConfigurationProperties.CLIENT_PID);
		assertNotNull(clientConfig.getProperties());
		
		// now track the MongoClientProvider service
		final CountDownLatch createLatch = new CountDownLatch(1);
		final CountDownLatch removeLatch = new CountDownLatch(1);
		MongoProviderCustomizer<MongoClientProvider, MongoClientProvider> customizer = new MongoProviderCustomizer<MongoClientProvider, MongoClientProvider>(context, createLatch, removeLatch);
		ServiceTracker<MongoClientProvider, MongoClientProvider> tracker = new ServiceTracker<MongoClientProvider, MongoClientProvider>(context, MongoClientProvider.class, customizer);
		tracker.open(true);
		
		// wait maximum 5 second until then the service must be created
		createLatch.await(5, TimeUnit.SECONDS);
		assertEquals(0, createLatch.getCount());
		
		// get the MongoClientProvider
		ServiceReference<?>[] clientRefs = context.getAllServiceReferences(MongoClientProvider.class.getName(), null);
		assertNotNull(clientRefs);
		assertEquals(1, clientRefs.length);
		ServiceReference<?> clientRef = clientRefs[0];
		MongoClientProvider clientProvider = (MongoClientProvider) context.getService(clientRef);
		assertEquals(clientId, clientProvider.getClientId());
		assertEquals(1, clientProvider.getURIs().length);
		assertEquals(clientUri, clientProvider.getURIs()[0]);
		
		// remove configuration
		clientConfig = cm.getConfiguration(ConfigurationProperties.CLIENT_PID);
		assertNotNull(clientConfig.getProperties());
		clientConfig.delete();
		removeLatch.await(5, TimeUnit.SECONDS);
		assertEquals(1, customizer.getRemoveCount());
		assertNull(tracker.getService());
		clientRefs = context.getAllServiceReferences(MongoClientProvider.class.getName(), null);
		assertNull(clientRefs);
	}
	
	@Test
	public void testModifyMongoClientProvider() throws InvalidSyntaxException, BundleException, IOException, InterruptedException {
		// get some of the bundles to test, to get the bundle context
		Bundle bundle = FrameworkUtil.getBundle(MongoDatabaseProvider.class);
		assertEquals("org.eclipselabs.mongo.osgi.api", bundle.getSymbolicName());
		bundle.start();
		assertEquals(Bundle.ACTIVE, bundle.getState());
		
		// get bundle context
		BundleContext context = bundle.getBundleContext();
		assertNotNull(context);
		
		// service lookup for configuration admin service
		ServiceReference<?>[] allServiceReferences = context.getAllServiceReferences(ConfigurationAdmin.class.getName(), null);
		assertNotNull(allServiceReferences);
		assertEquals(1, allServiceReferences.length);
		ServiceReference<?> cmRef = allServiceReferences[0];
		Object service = context.getService(cmRef);
		assertNotNull(service);
		assertTrue(service instanceof ConfigurationAdmin);
		
		// create mongo client configuration
		ConfigurationAdmin cm = (ConfigurationAdmin) service;
		Configuration clientConfig = cm.getConfiguration(ConfigurationProperties.CLIENT_PID);
		assertNotNull(clientConfig);
		
		// has to be a new configuration
		Dictionary<String, Object> p = clientConfig.getProperties();
		assertNull(p);
		
		// add service properties
		String clientId = "testClient";
		String clientUri = "mongodb://localhost:27017";
		p = new Hashtable<String, Object>();
		p.put(MongoClientProvider.PROP_CLIENT_ID, clientId);
		p.put(MongoClientProvider.PROP_URI, clientUri);
		clientConfig.update(p);
		
		
		// now track the MongoClientProvider service
		final CountDownLatch createLatch = new CountDownLatch(1);
		final CountDownLatch modifyLatch = new CountDownLatch(1);
		final CountDownLatch removeLatch = new CountDownLatch(1);
		MongoProviderCustomizer<MongoClientProvider, MongoClientProvider> customizer = new MongoProviderCustomizer<MongoClientProvider, MongoClientProvider>(context, createLatch, removeLatch, modifyLatch);
		ServiceTracker<MongoClientProvider, MongoClientProvider> tracker = new ServiceTracker<MongoClientProvider, MongoClientProvider>(context, MongoClientProvider.class, customizer);
		tracker.open(true);
		
		// re-check configuration
		clientConfig = cm.getConfiguration(ConfigurationProperties.CLIENT_PID);
		assertNotNull(clientConfig.getProperties());
		
		// wait maximum 5 second until then the service must be created
		createLatch.await(5, TimeUnit.SECONDS);
		assertEquals(0, createLatch.getCount());
		
		// get the MongoClientProvider
		ServiceReference<?>[] clientRefs = context.getAllServiceReferences(MongoClientProvider.class.getName(), null);
		assertNotNull(clientRefs);
		assertEquals(1, clientRefs.length);
		ServiceReference<?> clientRef = clientRefs[0];
		MongoClientProvider clientProvider = (MongoClientProvider) context.getService(clientRef);
		assertEquals(clientId, clientProvider.getClientId());
		assertEquals(1, clientProvider.getURIs().length);
		assertEquals(clientUri, clientProvider.getURIs()[0]);
		
		// remove configuration
		clientConfig = cm.getConfiguration(ConfigurationProperties.CLIENT_PID);
		assertNotNull(clientConfig.getProperties());
		p = new Hashtable<String, Object>();
		p.put(MongoClientProvider.PROP_CLIENT_ID, clientId + "2");
		p.put(MongoClientProvider.PROP_URI, clientUri + "2");
		clientConfig.update(p);
		
		modifyLatch.await(5, TimeUnit.SECONDS);
		assertEquals(1, customizer.getModifyCount());
		clientProvider = tracker.getService();
		assertNotNull(clientProvider);
		assertEquals(clientId + "2", clientProvider.getClientId());
		assertEquals(1, clientProvider.getURIs().length);
		assertEquals(clientUri + "2", clientProvider.getURIs()[0]);
		
		clientConfig.delete();
		removeLatch.await(5, TimeUnit.SECONDS);
		assertEquals(1, customizer.getRemoveCount());
		assertNull(tracker.getService());
		clientRefs = context.getAllServiceReferences(MongoClientProvider.class.getName(), null);
		assertNull(clientRefs);
		
		assertEquals(1, customizer.getModifyCount());
		assertEquals(1, customizer.getAddCount());
	}

}
