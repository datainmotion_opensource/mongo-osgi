package org.eclipselabs.mongo.osgi.integration.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.Dictionary;
import java.util.Hashtable;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import org.eclipselabs.mongo.osgi.api.MongoClientProvider;
import org.eclipselabs.mongo.osgi.api.MongoDatabaseProvider;
import org.eclipselabs.mongo.osgi.configuration.ConfigurationProperties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.BundleException;
import org.osgi.framework.FrameworkUtil;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.framework.ServiceReference;
import org.osgi.service.cm.Configuration;
import org.osgi.service.cm.ConfigurationAdmin;
import org.osgi.util.tracker.ServiceTracker;

import com.mongodb.client.MongoDatabase;

public class MongoDatabaseProviderOSGiTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testNoMongoClientProvider() throws InvalidSyntaxException, BundleException, IOException, InterruptedException {
		// get some of the bundles to test, to get the bundle context
		Bundle bundle = FrameworkUtil.getBundle(MongoDatabaseProvider.class);
		assertEquals("org.eclipselabs.mongo.osgi.api", bundle.getSymbolicName());
		bundle.start();
		assertEquals(Bundle.ACTIVE, bundle.getState());
		
		// get bundle context
		BundleContext context = bundle.getBundleContext();
		assertNotNull(context);
		
		// service lookup for configuration admin service
		ServiceReference<?>[] allServiceReferences = context.getAllServiceReferences(ConfigurationAdmin.class.getName(), null);
		assertNotNull(allServiceReferences);
		assertEquals(1, allServiceReferences.length);
		ServiceReference<?> cmRef = allServiceReferences[0];
		Object service = context.getService(cmRef);
		assertNotNull(service);
		assertTrue(service instanceof ConfigurationAdmin);
		
		// create mongo client configuration
		ConfigurationAdmin cm = (ConfigurationAdmin) service;
		Configuration databaseConfig = cm.getConfiguration(ConfigurationProperties.DATABASE_PID);
		assertNotNull(databaseConfig);
		
		// has to be a new configuration
		Dictionary<String, Object> p = databaseConfig.getProperties();
		assertNull(p);
		
		// add service properties
		String dbAlias = "testDB";
		String db = "test";
		p = new Hashtable<String, Object>();
		p.put(MongoDatabaseProvider.PROP_ALIAS, dbAlias);
		p.put(MongoDatabaseProvider.PROP_DATABASE, db);
		databaseConfig.update(p);
		
		// re-check configuration must not available, because of the missing client
		databaseConfig = cm.getConfiguration(ConfigurationProperties.CLIENT_PID);
		assertNull(databaseConfig.getProperties());
		
		databaseConfig.delete();
		
	}
	
	@Test
	public void testCreateMongoDatabaseProvider() throws InvalidSyntaxException, BundleException, IOException, InterruptedException {
		// get some of the bundles to test, to get the bundle context
		Bundle bundle = FrameworkUtil.getBundle(MongoDatabaseProvider.class);
		assertEquals("org.eclipselabs.mongo.osgi.api", bundle.getSymbolicName());
		bundle.start();
		assertEquals(Bundle.ACTIVE, bundle.getState());
		
		// get bundle context
		BundleContext context = bundle.getBundleContext();
		assertNotNull(context);
		
		// service lookup for configuration admin service
		ServiceReference<?>[] allServiceReferences = context.getAllServiceReferences(ConfigurationAdmin.class.getName(), null);
		assertNotNull(allServiceReferences);
		assertEquals(1, allServiceReferences.length);
		ServiceReference<?> cmRef = allServiceReferences[0];
		Object service = context.getService(cmRef);
		assertNotNull(service);
		assertTrue(service instanceof ConfigurationAdmin);
		
		// create mongo client configuration
		ConfigurationAdmin cm = (ConfigurationAdmin) service;
		Configuration clientConfig = cm.getConfiguration(ConfigurationProperties.CLIENT_PID);
		assertNotNull(clientConfig);
		
		// has to be a new configuration
		Dictionary<String, Object> p = clientConfig.getProperties();
		assertNull(p);
		
		// add service properties
		String clientId = "testClient";
		String clientUri = "mongodb://localhost:27017";
		p = new Hashtable<String, Object>();
		p.put(MongoClientProvider.PROP_CLIENT_ID, clientId);
		p.put(MongoClientProvider.PROP_URI, clientUri);
		clientConfig.update(p);
		
		// now track the MongoClientProvider service
		final CountDownLatch createLatch = new CountDownLatch(1);
		MongoProviderCustomizer<MongoClientProvider, MongoClientProvider> customizer = new MongoProviderCustomizer<MongoClientProvider, MongoClientProvider>(context, createLatch);
		ServiceTracker<MongoClientProvider, MongoClientProvider> tracker = new ServiceTracker<MongoClientProvider, MongoClientProvider>(context, MongoClientProvider.class, customizer);
		tracker.open(true);
		
		// re-check configuration
		clientConfig = cm.getConfiguration(ConfigurationProperties.CLIENT_PID);
		assertNotNull(clientConfig.getProperties());
		
		// wait maximum 5 second until then the service must be created
		createLatch.await(5, TimeUnit.SECONDS);
		assertEquals(0, createLatch.getCount());
		
		// create data base provider configuration
		Configuration databaseConfig = cm.getConfiguration(ConfigurationProperties.DATABASE_PID);
		assertNotNull(databaseConfig);
		
		// add service properties
		String dbAlias = "testDB";
		String db = "test";
		Dictionary<String, Object> dbp = new Hashtable<String, Object>();
		dbp.put(MongoDatabaseProvider.PROP_ALIAS, dbAlias);
		dbp.put(MongoDatabaseProvider.PROP_DATABASE, db);
		databaseConfig.update(dbp);
		
		// now track the MongoDatabaseProvider service
		CountDownLatch dbCreateLatch = new CountDownLatch(1);
		MongoProviderCustomizer<MongoDatabaseProvider, MongoDatabaseProvider> dbCustomizer = new MongoProviderCustomizer<MongoDatabaseProvider, MongoDatabaseProvider>(context, dbCreateLatch);
		ServiceTracker<MongoDatabaseProvider, MongoDatabaseProvider> dbTracker = new ServiceTracker<MongoDatabaseProvider, MongoDatabaseProvider>(context, MongoDatabaseProvider.class, dbCustomizer);
		dbTracker.open(true);
		
		// wait maximum 5 second until then the service must be created
		dbCreateLatch.await(5, TimeUnit.SECONDS);
		assertEquals(0, dbCreateLatch.getCount());
		
		// get the MongoDatabaseProvider
		ServiceReference<?>[] databaseRefs = context.getAllServiceReferences(MongoDatabaseProvider.class.getName(), null);
		assertNotNull(databaseRefs);
		assertEquals(1, databaseRefs.length);
		ServiceReference<?> databaseRef = databaseRefs[0];
		MongoDatabaseProvider databaseProvider = (MongoDatabaseProvider) context.getService(databaseRef);
		MongoDatabase database = databaseProvider.getDatabase();
		assertEquals(db, database.getName());
		
		databaseConfig.delete();
		clientConfig.delete();
	}

}
